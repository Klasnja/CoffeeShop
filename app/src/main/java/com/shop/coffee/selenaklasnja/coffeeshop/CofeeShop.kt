package com.shop.coffee.selenaklasnja.coffeeshop

import java.util.concurrent.Executors
import java.util.concurrent.LinkedBlockingQueue

/**
 * Created by selenaklasnja on 6/6/18.
 */


class CoffeeShop(val stuff: Stuff) : Runnable {

    val executorService =
            Executors.newFixedThreadPool(Stuff.TAKE_ORDERS + Stuff.MACHINES + Stuff.DELIVERER + 3)

    override fun run() {
        try {
            while (true) {
                val operation = operationsQueue.take()
                operation.consume(executorService)
            }
        } catch (ex: InterruptedException) {
            println("InterruptedException")
        }
    }

    val operationsQueue = LinkedBlockingQueue<Operation>()

    fun placeOrder(operation: Operation) {
        operationsQueue.put(operation)
    }
}