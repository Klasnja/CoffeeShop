package com.shop.coffee.selenaklasnja.coffeeshop

import java.util.concurrent.Semaphore

/**
 * Created by selenaklasnja on 6/6/18.
 */
class Stuff {

    companion object {
        const val TAKE_ORDERS = 1
        const val MACHINES = 2
        const val DELIVERER = 1
    }

    val takeOrders = TakeOrders()
    val coffeeMachines = CoffeeMachines()
    val deliverers = Deliverers()
}

open class StuffGroup(val semaphore: Semaphore) {
    fun acquire() = semaphore.acquire()
    fun release() = semaphore.release()
}

class TakeOrders : StuffGroup(Semaphore(Stuff.TAKE_ORDERS, true))
class CoffeeMachines : StuffGroup(Semaphore(Stuff.MACHINES, true))
class Deliverers : StuffGroup(Semaphore(Stuff.DELIVERER, true))
