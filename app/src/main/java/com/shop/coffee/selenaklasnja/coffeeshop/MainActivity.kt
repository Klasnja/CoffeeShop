package com.shop.coffee.selenaklasnja.coffeeshop

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val coffeeShop= CoffeeShop(Stuff())
    var id: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        findViewById<Button>(R.id.coffee_with_milk).setOnClickListener { view ->
            coffeeShop.placeOrder((Operation.create(coffeeShop.stuff, Customer(id++, "Tom"), CoffeeType.ESPRESO, true)))
        }

        findViewById<Button>(R.id.coffee).setOnClickListener { view ->
            coffeeShop.placeOrder((Operation.create(coffeeShop.stuff, Customer(id++, "Tom"), CoffeeType.ESPRESO, false)))
        }


        Thread(coffeeShop).start()

    }


}
