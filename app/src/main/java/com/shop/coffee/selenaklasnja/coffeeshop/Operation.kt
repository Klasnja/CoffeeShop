package com.shop.coffee.selenaklasnja.coffeeshop

import java.util.concurrent.ExecutorService

/**
 * Created by selenaklasnja on 6/6/18.
 */
class Operation(val currentRole: Role) {
    companion object {
        fun create(stuff: Stuff, customer: Customer, coffeeType: CoffeeType, isWithMilk: Boolean): Operation {
            val roles = mutableListOf<Role>()

            val startRole = AcceptOrder(customer, stuff.takeOrders)
            roles.add(startRole)
            roles.add(PourCoffee(customer, stuff.coffeeMachines, coffeeType, isWithMilk))
            roles.add(PassOrder(customer, stuff.deliverers))

            val size = roles.size

            for (i in roles.indices) {
                if (i < size - 1) {
                    roles[i].outputRole = roles[i + 1]
                }
                if (i > 1) {
                    roles[i].inputRole = roles[i - 1]
                }
            }
            return Operation(startRole)
        }
    }

    fun consume(executorService: ExecutorService) {
        currentRole.execute(executorService)
    }
}

data class Customer(val id: Long, val name: String? = null)
