package com.shop.coffee.selenaklasnja.coffeeshop

import java.util.concurrent.ExecutorService

/**
 * Created by selenaklasnja on 6/6/18.
 */
abstract class Role(val stuffGroup: StuffGroup, val task: () -> Unit) {

    var inputRole: Role? = null
    var outputRole: Role? = null


    fun execute(executorService: ExecutorService) {
        executorService.submit {
            stuffGroup.acquire()
            task.invoke()
            stuffGroup.release()
            outputRole?.execute(executorService)
        }
    }
}

class AcceptOrder(customer: Customer, stuffGroup: StuffGroup) : Role(stuffGroup,
        {
            println("${customer.id} - Order taking started")
            Thread.sleep((2000 * Math.random()).toLong())
            println("${customer.id} - Order has been accepted")
        })

enum class CoffeeType {
    ESPRESO
}

class PourCoffee(customer: Customer, stuffGroup: StuffGroup, val coffeeType: CoffeeType,
                 val isWithMilk: Boolean) : Role(stuffGroup,
        {
            println("${customer.id} - Coffee $coffeeType has been started")
            Thread.sleep((2000 * Math.random()).toLong() + 5000)
            println("${customer.id} - Coffee $coffeeType has been poured")

            if (isWithMilk) {
                println("${customer.id} - Milk has been started")
                Thread.sleep((2000 * Math.random()).toLong() + 3000)
                println("${customer.id} - Milk has been poured")
            }
        })

class PassOrder(customer: Customer, stuffGroup: StuffGroup) : Role(stuffGroup,
        {
            println("${customer.id} - Delivering started")
            Thread.sleep((2000 * Math.random()).toLong())
            println("${customer.id} - Delivering finished")
        })
